/* @flow */

// Instruments
import { resolve } from 'path';

// Core
export const source = resolve(__dirname, '../source');
export const build = resolve(__dirname, '../build');

// Assets
export const favicon = resolve(__dirname, '../source/theme/images/favicon.png');

// antd
export const antd = resolve(__dirname, '../source/theme/antd');
