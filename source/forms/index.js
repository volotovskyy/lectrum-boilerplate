import { LoginForm } from './LoginForm';
import TestForm from './TestForm';
import ReactReduxForm from './ReactReduxForm';
import { RequestForm } from './RequestForm';
import { FormikForm } from './FormikForm';
import { ProfileForm } from './ProfileForm';
import { OrderForm } from './OrderForm';
import { AddOrderForm } from './AddOrderForm';
import AntReduxForm from './AntReduxForm';

export {
    TestForm,
    LoginForm,
    ReactReduxForm,
    RequestForm,
    FormikForm,
    ProfileForm,
    OrderForm,
    AddOrderForm,
    AntReduxForm,
};
