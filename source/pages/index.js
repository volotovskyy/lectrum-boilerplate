import OrdersPage from './OrdersPage';
import OrderPage from './OrderPage';
import ProfilePage from './ProfilePage';
import ExceptionPage from './ExceptionPage';
import AddOrderPage from './AddOrderPage';

export { OrdersPage, OrderPage, AddOrderPage, ProfilePage, ExceptionPage };
