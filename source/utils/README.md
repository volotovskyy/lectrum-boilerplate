# utils

---

## api

fetchAPI function handles all api calls and exceptions

## ConnectedIntlProvider

connected to redux store react-intl provider

---

### sideEffects

SideEffects services and helpers

*   localStorage
*   etc.

### images

Handle import images to React components

### tools

helper tools
