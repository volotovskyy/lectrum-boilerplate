// Layout
import carbookLogo from 'theme/images/carbookLogo.png';
import exception400 from 'theme/images/exceptions/400.svg';
import exception404 from 'theme/images/exceptions/404.svg';
import exception500 from 'theme/images/exceptions/500.svg';

export default {
    // Layout
    carbookLogo,
    // Exceptions
    exception400,
    exception404,
    exception500,
};
