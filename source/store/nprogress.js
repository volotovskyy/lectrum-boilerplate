// Core
import nprogress from 'nprogress';

nprogress.configure({
    showSpinner: false,
});
