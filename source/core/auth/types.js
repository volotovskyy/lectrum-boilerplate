export default Object.freeze({
    // login
    LOGIN:         'LOGIN',
    LOGIN_SUCCESS: 'LOGIN_SUCCESS',
    LOGIN_FAIL:    'LOGIN_FAIL',

    //logout
    LOGOUT:         'LOGOUT',
    LOGOUT_SUCCESS: 'LOGOUT_SUCCESS',
    LOGOUT_FAIL:    'LOGOUT_FAIL',
});
