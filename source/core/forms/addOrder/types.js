export default Object.freeze({
    CHANGE:     'CHANGE',
    SET_ERRORS: 'SET_ERRORS',
    RESET:      'RESET',
});
