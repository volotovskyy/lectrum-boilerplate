export default Object.freeze({
    // fetchORDERs
    FETCH_ORDER:         'FETCH_ORDER',
    FETCH_ORDER_SUCCESS: 'FETCH_ORDER_SUCCESS',
    FETCH_ORDER_FAIL:    'FETCH_ORDER_FAIL',
});
