export default Object.freeze({
    SET_FIRST_NAME: 'SET_FIRST_NAME',
    SET_LAST_NAME:  'SET_LAST_NAME',
    SET_EMAIL:      'SET_EMAIL',
    SET_PASSWORD:   'SET_PASSWORD',
    SET_PHONE:      'SET_PHONE',
    SET_AVATAR:     'SET_AVATAR',
    SET_LANG:       'SET_LANG',
});
