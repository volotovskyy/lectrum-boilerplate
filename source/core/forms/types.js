// Types
export const DOMAIN = 'forms';
export const CHANGE = 'CHANGE';
export const SET_ERRORS = 'SET_ERRORS';
export const RESET = 'RESET';
